package android.ojedas.crystallball;

class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions(){
        answers = new String[] {
                "Your wishes will come true.",
                "Your wishes will never come true.",
                "Come back Tomorrow for an answer.",
                "No",
                "yes",
                "Maybe",
                "Fix your own problems",
                "Go live your life",
                "Sure why not",
                "Good things will happen"
        };
    }

    public static Predictions get() {
        if(predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction(){
        int random = (int ) (Math. random() * 10 + 1);
        return answers[random];
    }
}
