package android.ojedas.crystallball;

import android.support.v7.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.FloatMath;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.view.Menu;
import android.widget.ImageView;

public class CrystalBall extends AppCompatActivity {
    private TextView answerText;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private float acceleration;
    private float currentAcceleration;
    private float previousAcceleration;

    private final SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            previousAcceleration = currentAcceleration;
            currentAcceleration = FloatMath.sqrt(x * x + y * y + z * z);
            float delta = currentAcceleration - previousAcceleration;
            acceleration = acceleration * 0.9f + delta;

            if(acceleration > 8){
                Toast toast = Toast.makeText(getApplication(), "Device has shaken", Toast.LENGTH_SHORT);
                toast.show();

                class SplashActivity extends Activity {
                    ImageView iv1;
                    AnimationDrawable Anim;
                    @Override
                    protected void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        setContentView(R.layout.activity_crystal_ball);
                        iv1 = (ImageView) findViewById(R.id.ball01);
                        try {
                            BitmapDrawable frame1 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball02);
                            BitmapDrawable frame2 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball03);
                            BitmapDrawable frame3 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball04);
                            BitmapDrawable frame4 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball05);
                            BitmapDrawable frame5 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball06);
                            BitmapDrawable frame6 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball07);
                            BitmapDrawable frame7 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball08);
                            BitmapDrawable frame8 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball09);
                            BitmapDrawable frame9 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball10);
                            BitmapDrawable frame10 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball11);
                            BitmapDrawable frame11 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball12);
                            BitmapDrawable frame12 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball13);
                            BitmapDrawable frame13 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball14);
                            BitmapDrawable frame14 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball15);
                            BitmapDrawable frame15 = (BitmapDrawable) getResources().getDrawable(
                                    R.drawable.ball16);



                            Anim = new AnimationDrawable();
                            Anim.addFrame(frame1, 200);
                            Anim.addFrame(frame2, 200);
                            Anim.addFrame(frame3, 200);
                            Anim.addFrame(frame4, 200);
                            Anim.addFrame(frame5, 200);
                            Anim.addFrame(frame6, 200);
                            Anim.addFrame(frame7, 200);
                            Anim.addFrame(frame8, 200);
                            Anim.addFrame(frame9, 200);
                            Anim.addFrame(frame10, 200);
                            Anim.addFrame(frame11, 200);
                            Anim.addFrame(frame12, 200);
                            Anim.addFrame(frame13, 200);
                            Anim.addFrame(frame14, 200);
                            Anim.addFrame(frame15, 200);


                            Anim.setOneShot(false);
                            iv1.setBackgroundDrawable(Anim);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    Anim.start();
                                }
                            }, 5000);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                    }
                }

                answerText = (TextView) findViewById(R.id.answerText);
                answerText.setText(Predictions.get().getPrediction());

                MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.crystal_ball);
                mediaPlayer.start();
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crystal_ball);

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        acceleration = 0.0f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;
        previousAcceleration = SensorManager.GRAVITY_EARTH;





    }
    @Override
    protected void onResume(){
        super.onResume();

        sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause(){
        super.onPause();

        sensorManager.unregisterListener(sensorListener);
    }

}
